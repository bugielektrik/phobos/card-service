// nolint: funlen
package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/hashicorp/go-hclog"

	"gitlab.com/bugielektrik/phobos/card-service/internal/api"
	"gitlab.com/bugielektrik/phobos/card-service/internal/config"
	"gitlab.com/bugielektrik/phobos/card-service/internal/service"
	"gitlab.com/bugielektrik/phobos/card-service/internal/store/postgres"
	"gitlab.com/bugielektrik/phobos/card-service/pkg/database"
	"gitlab.com/bugielektrik/phobos/card-service/pkg/logger"
)

const (
	configsDir = "configs"
	project    = "card"
)

// Run initializes whole application.
func main() {
	hcLogger := hclog.New(&hclog.LoggerOptions{
		JSONFormat: false,
		Level:      hclog.Debug,
	})

	cfg, err := config.Init(configsDir, project)
	if err != nil {
		logger.Error(err)
		return
	}

	// Dependencies
	postgresDB, err := database.New(cfg.Postgres.DataSourceName, project)
	if err != nil {
		logger.Error(err)
		return
	}

	err = database.Migrate(cfg.Postgres.DataSourceName)
	if err != nil {
		logger.Error(err)
		return
	}

	// Services, Repos & API Handlers
	stores := postgres.New(postgresDB, hcLogger)

	services := service.New(service.Dependencies{
		CardStore: stores.Card,
	})

	handlers := api.New(api.Dependencies{
		CardService: services.Card,
	})

	// HTTP Server
	server := handlers.InitRest(cfg)

	// Listen from a different goroutine
	go func() {
		if err := server.Listen(":" + cfg.HTTP.Port); err != nil {
			log.Panic(err)
		}
	}()

	logger.Info("Server started")

	// Graceful Shutdown
	quit := make(chan os.Signal, 1)                    // Create channel to signify a signal being sent
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM) // When an interrupt or termination signal is sent, notify the channel

	_ = <-quit // This blocks the main thread until an interrupt is received
	fmt.Println("Gracefully shutting down...")
	_ = server.Shutdown()

	fmt.Println("Running cleanup tasks...")
	// Your cleanup tasks go here
	if err := postgresDB.Close(); err != nil {
		logger.Error(err.Error())
	}

	fmt.Println("Fiber was successful shutdown.")
}
