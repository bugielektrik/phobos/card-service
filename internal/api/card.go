package api

import (
	"context"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"

	"gitlab.com/bugielektrik/phobos/card-service/internal/model"
)

type CardService interface {
	Create(ctx context.Context, accountID string, card *model.Card) (*model.Card, error)
	GetAll(accountID string) ([]*model.Card, error)
	GetByID(id, accountID string) (*model.Card, error)
	Update(ctx context.Context, id, accountID string, card *model.CardInstance) (*model.Card, error)
	Delete(ctx context.Context, id, accountID string) (*model.Card, error)
}

type Card struct {
	service CardService
}

func NewCardHandler(s CardService) *Card {
	return &Card{service: s}
}

func (h *Card) createCard(c *fiber.Ctx) error {
	// Bind params
	accountID := c.Get("X-Account-ID")

	cardDest := new(model.Card)
	if err := c.BodyParser(cardDest); err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	// Validate params
	if err := validator.New().Struct(cardDest); err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	// Write data to DB
	cardSrc, err := h.service.Create(c.Context(), accountID, cardDest)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	c.Status(fiber.StatusCreated)
	return c.JSON(cardSrc)
}

func (h *Card) getCards(c *fiber.Ctx) error {
	// Bind params
	accountID := c.Get("X-Account-ID")

	// Read data from database
	cardsSrc, err := h.service.GetAll(accountID)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	return c.JSON(cardsSrc)
}

func (h *Card) getCardByID(c *fiber.Ctx) error {
	// Bind params
	cardID := c.Params("id")
	accountID := c.Get("X-Account-ID")

	// Read data from database
	cardSrc, err := h.service.GetByID(cardID, accountID)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	if cardSrc == nil {
		return c.SendStatus(fiber.StatusNotFound)
	}

	return c.JSON(cardSrc)
}

func (h *Card) updateCard(c *fiber.Ctx) error {
	// Bind params
	cardID := c.Params("id")
	accountID := c.Get("X-Account-ID")

	// Bind request body
	cardDest := new(model.CardInstance)
	if err := c.BodyParser(cardDest); err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	// Write data to DB
	ctx := context.WithValue(c.Context(), "id", cardID)
	cardSrc, err := h.service.Update(ctx, cardID, accountID, cardDest)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	if cardSrc == nil {
		return c.SendStatus(fiber.StatusNotFound)
	}

	return c.SendStatus(http.StatusOK)
}

func (h *Card) deleteCard(c *fiber.Ctx) error {
	// Bind params
	cardID := c.Params("id")
	accountID := c.Get("X-Account-ID")

	// Delete data from DB
	ctx := context.WithValue(c.Context(), "id", cardID)
	cardSrc, err := h.service.Delete(ctx, cardID, accountID)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	if cardSrc == nil {
		return c.SendStatus(fiber.StatusNotFound)
	}

	return c.SendStatus(http.StatusOK)
}
