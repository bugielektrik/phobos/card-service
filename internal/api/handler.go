package api

type Dependencies struct {
	CardService CardService
}

type Handler struct {
	Card *Card
}

func New(d Dependencies) *Handler {
	return &Handler{
		Card: NewCardHandler(d.CardService),
	}
}
