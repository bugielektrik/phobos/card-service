package service

import (
	"context"

	"github.com/jmoiron/sqlx"

	"gitlab.com/bugielektrik/phobos/card-service/internal/model"
)

type CardStore interface {
	Create(data *model.Card) error
	Get(id string) (*model.Card, error)
	GetAll(accountID string) ([]*model.Card, error)
	GetByID(id, accountID string) (*model.Card, error)
	Delete(id, accountID string) error
	Lock(ctx context.Context, id, accountID string) (func(), *sqlx.Tx, error)
	Update(ctx context.Context, tx *sqlx.Tx, id, accountID string, data *model.CardInstance) error
}

type Card struct {
	store CardStore
}

func NewCardService(s CardStore) *Card {
	return &Card{store: s}
}

func (s *Card) Create(c context.Context, accountID string, cardDest *model.Card) (*model.Card, error) {
	// If it is first data set as a main
	cardsSrc, err := s.store.GetAll(cardDest.AccountID)
	if err != nil {
		return nil, err
	}

	if len(cardsSrc) == 0 {
		cardDest.IsMain = true
	} else {
		// If it is main data, find old main data and change it value to false
		if cardDest.IsMain == true {
			for i := 0; i < len(cardsSrc); i++ {
				if cardsSrc[i].ID == cardDest.ID || cardsSrc[i].IsMain == false {
					continue
				}
				cardIns := &model.CardInstance{
					ID:     cardsSrc[i].ID,
					IsMain: &[]bool{false}[0],
				}

				ctx := context.WithValue(c, "id", cardIns.ID)

				unlockCard, reqTx, err := s.store.Lock(ctx, cardIns.ID, cardDest.AccountID)
				if err != nil {
					return nil, err
				}

				err = s.store.Update(ctx, reqTx, cardIns.ID, cardDest.AccountID, cardIns)
				if err != nil {
					return nil, err
				}

				unlockCard()
			}
		}
	}

	cardDest.AccountID = accountID
	err = s.store.Create(cardDest)
	return cardDest, err
}

func (s *Card) GetByID(id, accountID string) (*model.Card, error) {
	cardSrc, err := s.store.GetByID(id, accountID)
	return cardSrc, err
}

func (s *Card) GetAll(accountID string) ([]*model.Card, error) {
	cardsSrc, err := s.store.GetAll(accountID)
	return cardsSrc, err
}

func (s *Card) Update(ctx context.Context, id, accountID string, cardDest *model.CardInstance) (*model.Card, error) {
	cardSrc, err := s.store.GetByID(id, accountID)
	if err != nil {
		return nil, err
	}

	if cardSrc == nil {
		return nil, nil
	}

	// If it is main data, find old main data and change it value to false
	if *cardDest.IsMain == true {
		cardsSrc, err := s.store.GetAll(accountID)
		if err != nil {
			return nil, err
		}

		for i := 0; i < len(cardsSrc); i++ {
			if cardsSrc[i].ID == id || cardsSrc[i].IsMain == false {
				continue
			}
			cardIns := &model.CardInstance{
				ID:     cardsSrc[i].ID,
				IsMain: &[]bool{false}[0],
			}

			unlockCard, reqTx, err := s.store.Lock(ctx, cardIns.ID, accountID)
			if err != nil {
				return nil, err
			}

			err = s.store.Update(ctx, reqTx, cardIns.ID, accountID, cardIns)
			if err != nil {
				return nil, err
			}

			unlockCard()
		}
	}

	unlockCard, reqTx, err := s.store.Lock(ctx, id, accountID)
	if err != nil {
		return nil, err
	}
	defer unlockCard()

	err = s.store.Update(ctx, reqTx, id, accountID, cardDest)
	if err != nil {
		return nil, err
	}

	return cardSrc, nil
}

func (s *Card) Delete(ctx context.Context, id, accountID string) (*model.Card, error) {
	cardSrc, err := s.store.GetByID(id, accountID)
	if err != nil {
		return nil, err
	}

	if cardSrc == nil {
		return nil, nil
	}

	// If it is main data, get first data and change it to main
	if cardSrc.IsMain == true {
		cardsSrc, err := s.store.GetAll(accountID)
		if err != nil {
			return nil, err
		}

		if len(cardsSrc) > 0 {
			for i := 0; i < len(cardsSrc); i++ {
				if cardsSrc[i].ID == id {
					continue
				}
				cardIns := &model.CardInstance{
					ID:     cardsSrc[i].ID,
					IsMain: &[]bool{true}[0],
				}

				unlockCard, reqTx, err := s.store.Lock(ctx, cardIns.ID, accountID)
				if err != nil {
					return nil, err
				}

				err = s.store.Update(ctx, reqTx, cardIns.ID, accountID, cardIns)
				if err != nil {
					return nil, err
				}

				unlockCard()

				break
			}
		}
	}

	err = s.store.Delete(id, accountID)
	if err != nil {
		return nil, err
	}

	return cardSrc, nil
}
