package service

type Dependencies struct {
	CardStore CardStore
}

type Service struct {
	Card *Card
}

func New(d Dependencies) *Service {
	return &Service{
		Card: NewCardService(d.CardStore),
	}
}
