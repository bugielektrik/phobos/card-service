package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/hashicorp/go-hclog"
	"github.com/jmoiron/sqlx"

	"gitlab.com/bugielektrik/phobos/card-service/internal/model"
)

type Card struct {
	db     *sqlx.DB
	logger hclog.Logger
}

func NewCardStore(db *sqlx.DB, logger hclog.Logger) *Card {
	return &Card{
		db:     db,
		logger: logger,
	}
}

func (s *Card) Create(data *model.Card) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := s.db.NamedExecContext(ctx, `
		INSERT INTO cards (card_id, account_id, card_mask, card_type, issuer, name, is_main) 
		VALUES(:card_id, :account_id, :card_mask, :card_type, :issuer, :name, :is_main)`,
		data)
	return err
}

func (s *Card) Get(id string) (*model.Card, error) {
	query := `
		SELECT card_id, is_main
		FROM cards
		WHERE card_id=:card_id`
	args := map[string]interface{}{
		"card_id": id,
	}
	data, err := s.getRow("Get", query, args)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *Card) GetByID(id, accountID string) (*model.Card, error) {
	query := `
		SELECT *
		FROM cards
		WHERE card_id=:card_id AND account_id=:account_id`
	args := map[string]interface{}{
		"card_id":    id,
		"account_id": accountID,
	}
	data, err := s.getRow("GetByID", query, args)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *Card) GetAll(accountID string) ([]*model.Card, error) {
	query := `
		SELECT *
		FROM cards
		WHERE account_id='` + accountID + `'`
	data, err := s.getRows("GetAll", query)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *Card) Delete(id, accountID string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := s.db.NamedExecContext(ctx, `
		DELETE FROM cards
		WHERE card_id=:card_id AND account_id=:account_id`,
		map[string]interface{}{
			"card_id":    id,
			"account_id": accountID,
		})
	return err
}

func (s *Card) Update(ctx context.Context, tx *sqlx.Tx, id, accountID string, data *model.CardInstance) error {
	setValues := make([]string, 0)

	if data.IsMain != nil {
		setValues = append(setValues, "is_main=:is_main")
	}

	setValues = append(setValues, "updated_at=CURRENT_TIMESTAMP")

	setQuery := strings.Join(setValues, ", ")
	query := fmt.Sprintf("UPDATE cards SET %s WHERE card_id='%s' AND account_id='%s'", setQuery, id, accountID)

	err := s.update(ctx, tx, "Update", query, data)
	return err
}

func (s *Card) Lock(ctx context.Context, id, accountID string) (func(), *sqlx.Tx, error) {
	logger := s.logger.With("operation", "Lock", "id", id)

	query := `SELECT * FROM cards WHERE card_id=:card_id AND account_id=:account_id FOR UPDATE`
	args := map[string]interface{}{
		"card_id":    id,
		"account_id": accountID,
	}

	logger.Debug("begin transaction")
	tx, err := s.db.BeginTxx(ctx, nil)
	if err != nil {
		logger.Error("failed to begin transaction", "error", err)
		return nil, nil, err
	}

	rows, err := tx.NamedQuery(query, args)
	if err != nil {
		logger.Error("failed to execute statement", "error", err)
		return nil, nil, err
	}
	defer rows.Close()

	logger.Debug("locked")
	return func() {
		err := tx.Commit()
		if err != nil {
			logger.Error("error while unlocking", "error", err)
			return
		}

		logger.Debug("unlocked")
	}, tx, nil
}

func (s *Card) getRow(operation, query string, args interface{}) (*model.Card, error) {
	logger := s.logger.With("operation", operation)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	row, err := s.db.NamedQueryContext(ctx, query, args)
	if err != nil {
		logger.Error("failed to get", "error", err)
		return nil, err
	}
	defer row.Close()

	if !row.Next() {
		logger.Debug("not found")
		return nil, nil
	}

	data := new(model.Card)
	err = row.StructScan(&data)
	if err != nil {
		logger.Error("failed to scan into struct", "error", err)
		return nil, err
	}

	return data, nil
}

func (s *Card) getRows(operation, query string) ([]*model.Card, error) {
	logger := s.logger.With("operation", operation)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	rows, err := s.db.QueryxContext(ctx, query)
	if err == sql.ErrNoRows {
		logger.Debug("not found")
		return nil, nil
	} else if err != nil {
		logger.Error("failed to get", "error", err)
		return nil, err
	}
	defer rows.Close()

	dataArr := make([]*model.Card, 0)
	for rows.Next() {
		data := new(model.Card)
		err = rows.StructScan(&data)
		if err != nil {
			logger.Error("failed to scan into struct", "error", err)
			return nil, err
		}
		dataArr = append(dataArr, data)
	}

	return dataArr, nil
}

func (s *Card) update(ctx context.Context, tx *sqlx.Tx, operation, query string, args interface{}) error {
	logger := s.logger.With("operation", operation)

	logger.Debug("start exec")
	finalize := func() error { return nil }
	if tx == nil {
		ttx, err := s.db.BeginTxx(ctx, nil)
		if err != nil {
			// failed to start transaction
			logger.Error("failed to start transaction", "error", err)
			return err
		}
		finalize = func() error {
			err = ttx.Commit()
			if err != nil {
				logger.Error("failed to commit", "error", err)
				return err
			}

			logger.Debug("committed update")
			return nil
		}
		tx = ttx
	}

	_, err := tx.NamedExec(query, args)
	if err != nil {
		logger.Error("failed to update", "error", err)
		return err
	}

	logger.Info("updated")
	return finalize()
}
