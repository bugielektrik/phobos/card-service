package postgres

import (
	"github.com/hashicorp/go-hclog"
	"github.com/jmoiron/sqlx"
)

type Store struct {
	Card *Card
}

func New(db *sqlx.DB, logger hclog.Logger) *Store {
	return &Store{
		Card: NewCardStore(db, logger),
	}
}
