package model

import (
	"time"
)

type Card struct {
	InternalID string    `json:"-" db:"id"`
	AccountID  string    `json:"-" db:"account_id"`
	ID         string    `json:"card_id" db:"card_id" validate:"required"`
	CardMask   string    `json:"card_mask" db:"card_mask" validate:"required"`
	CardType   string    `json:"card_type" db:"card_type" validate:"required"`
	Issuer     string    `json:"issuer" db:"issuer" validate:"required"`
	Name       string    `json:"name" db:"name" validate:"required"`
	IsMain     bool      `json:"is_main" db:"is_main"`
	CreatedAt  time.Time `json:"created_at" db:"created_at"`
	UpdatedAt  time.Time `json:"updated_at" db:"updated_at"`
}

type CardInstance struct {
	ID     string `json:"card_id" db:"card_id"`
	IsMain *bool  `json:"is_main" db:"is_main"`
}
